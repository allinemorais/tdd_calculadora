package com.calculadora.calculadora;

import antlr.collections.List;
import org.assertj.core.api.IntegerAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class OperacoesTest {

    @Test
    public void TestarOPercaoDeSoma()
    {
        Assertions.assertEquals(Operacao.Soma(3,5),2);
    }

    @Test
    public void TestarOPercaoDeSomaLista()
    {
        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(10);
        numeros.add(2);
        numeros.add(1);
        Assertions.assertEquals(Operacao.SomarLista(numeros),13);
    }

    @Test
    public void TestarSubtracao()
    {
        Assertions.assertEquals(Operacao.Subtrair(3,5),2);
    }

    @Test
    public void TestarSubtracaoLlista()
    {
        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(10);
        numeros.add(2);
        numeros.add(1);
        Assertions.assertEquals(Operacao.SubtrairLista(numeros),7);
    }

    @Test
    public void TestarDivisao()
    {
        Assertions.assertEquals(Operacao.Divisao(3,5),2);
    }


    @Test
    public void TestarDivisaoLlista()
    {
        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(20);
        numeros.add(5);
        numeros.add(2);
        Assertions.assertEquals(Operacao.SubtrairLista(numeros),-27);
    }


    @Test
    public void TestarMultiplicacao()
    {
        Assertions.assertEquals(Operacao.Multiplicacao(3,5),2);
    }


    @Test
    public void TestarMultiplicacaoLlista()
    {
        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(10);
        numeros.add(2);
        numeros.add(1);
        Assertions.assertEquals(Operacao.MultiplicarLista(numeros),7);
    }


}
